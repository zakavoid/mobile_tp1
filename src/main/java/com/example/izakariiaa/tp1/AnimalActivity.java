package com.example.izakariiaa.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        final TextView tx1 = findViewById(R.id.title);
        final TextView tx11 = findViewById(R.id.textView11);
        final TextView tx22 = findViewById(R.id.textView22);
        final TextView tx33 = findViewById(R.id.textView33);
        final TextView tx44 = findViewById(R.id.textView44);
        final EditText tx5 = findViewById(R.id.editText);
        ImageView m = findViewById(R.id.IMG);


        Button btn = (Button) findViewById(R.id.button);

        Intent intent = getIntent();
        Bundle bn = intent.getBundleExtra("key");
        final String animal_name = bn.getString("anm");
        tx1.setText(animal_name);


        final Intent main=new Intent(this,MainActivity.class);
       AnimalList An = new AnimalList();
        final Animal n = An.getAnimal(animal_name);



        int id=getResources().getIdentifier("com.example.izakariiaa.tp1:drawable/"+n.getImgFile(),"null","null")  ;
        m.setImageResource(id);

        final String vm=n.getStrHightestLifespan();
        tx11.setText(vm);

        final String pg=n.getStrGestationPeriod();
        tx22.setText(pg);

        final String pn=n.getStrBirthWeight();
        tx33.setText(pn);


        final String pa=n.getStrAdultWeight();
        tx44.setText(pa);

        final String sc = n.getConservationStatus();
        tx5.setText(sc);



        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String an=String.valueOf(tx5.getText());
                n.setConservationStatus(an);
                startActivity(main);

            }
        });
    }
}
